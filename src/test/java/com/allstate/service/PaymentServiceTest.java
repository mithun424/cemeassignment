package com.allstate.service;

import com.allstate.entities.Payment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class PaymentServiceTest {
    @Autowired
    private PaymentService paymentService;

    @Test
    public void save_And_findById_Success() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment = new Payment(1, date, "CR", 10000d, 123);

        paymentService.save(payment);
        Payment paymentInfo = paymentService.findById(1);

        assertEquals(payment.getId(), paymentInfo.getId());

        String expectedResult = formatter.format(date);
        String actualResult = formatter.format(paymentInfo.getPaymentdate());
        assertEquals(expectedResult, actualResult);

        assertEquals(1, paymentService.findByType("CR").size());
        assertEquals(1, paymentService.rowcount());
    }
}
