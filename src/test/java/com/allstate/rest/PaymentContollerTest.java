package com.allstate.rest;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class PaymentContollerTest {
    @Mock
    private RestTemplate restTemplate;

    int port=8080;
    @Test
    public void getStatusShouldReturnDefaultMessage() throws Exception {
        Mockito.when(restTemplate.getForObject("http://localhost:" + port + "/api/payment/status",
                        String.class)).thenReturn("Rest Api is running");
    }
}
