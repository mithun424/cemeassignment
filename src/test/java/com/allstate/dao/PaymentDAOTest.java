package com.allstate.dao;

import com.allstate.entities.Payment;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class PaymentDAOTest {
    @Autowired
    private PaymentDAO dao;

    @Autowired
    MongoTemplate mongoTemplate;

    @Test
    public void save_And_findById_Success() {
        Payment payment = new Payment(1, new Date(), "CR", 10000d, 123);

        dao.save(payment);

        assertEquals(payment.getId(), dao.findByID(1).getId());
        assertEquals(1, dao.findByType("CR").size());
        assertEquals(1, dao.rowcount());
    }

    @AfterEach
    public  void cleanUp() {
        for (String collectionName : mongoTemplate.getCollectionNames()) {
            if (!collectionName.startsWith("system.")) {
                mongoTemplate.dropCollection(collectionName);
            }
        }

    }
}
