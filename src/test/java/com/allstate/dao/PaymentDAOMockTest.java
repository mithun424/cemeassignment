package com.allstate.dao;

import com.allstate.entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

public class PaymentDAOMockTest {
    @Mock
    private PaymentDAO dao;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void rowCount_success() {
        doReturn(1).when(dao).rowcount();
        assertEquals(1, dao.rowcount());
    }

    @Test
    public void save_Success() {
        Payment payment = new Payment(1, new Date(), "CR", 10000d, 123);
        doReturn(0).when(dao).save(any(Payment.class));
        assertEquals(0, dao.save(payment));
    }

    @Test
    public void findByID_Success() {
        Payment payment = new Payment(1, new Date(), "CR", 10000d, 123);
        doReturn(payment).when(dao).findByID(1);
        assertEquals(payment, dao.findByID(1));
    }

    @Test
    public void findByType_Success() {
        List<Payment> payments = new ArrayList<Payment>(Arrays.asList(
                new Payment(1, new Date(), "CR", 10000d, 123),
                new Payment(2, new Date(), "CR", 10000d, 124)
        ));
        doReturn(payments).when(dao).findByType("CR");
        assertEquals(payments, dao.findByType("CR"));
    }
}
