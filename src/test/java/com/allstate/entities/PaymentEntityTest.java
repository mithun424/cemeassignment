package com.allstate.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentEntityTest {
    private Payment payment;

    @BeforeEach
    void setUp() {
        payment = new Payment(1, new Date(), "CR", 10000d, 123);
    }
    @Test
    void getId() {
        assertEquals(1, payment.getId());
    }

    @Test
    void getPaymentdate() {
        assertEquals(new Date(), payment.getPaymentdate());
    }

    @Test
    void getType() {
        assertEquals("CR", payment.getType());
    }

    @Test
    void getAmount() { assertEquals(10000d, payment.getAmount());
    }

    @Test
    void getCustId() { assertEquals(123, payment.getCustId());
    }
}
