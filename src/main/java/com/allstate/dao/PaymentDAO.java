package com.allstate.dao;

import com.allstate.entities.Payment;
import java.util.List;

public interface PaymentDAO {
    int rowcount();
    List<Payment> getAllPayments();
    Payment findByID(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
}
