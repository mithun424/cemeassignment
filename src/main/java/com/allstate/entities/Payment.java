package com.allstate.entities;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class Payment {
    
    @Id
    private int id;
    private Date paymentdate;
    private String type;
    private double amount;
    private int custId;

    public Payment(int id, Date paymentdate, String type, double amount, int custId) {
        this.id = id;
        this.paymentdate = paymentdate;
        this.type = type;
        this.amount = amount;
        this.custId = custId;
    }

    public Payment() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentdate() {
        return paymentdate;
    }

    public void setPaymentdate(Date paymentdate) {
        this.paymentdate = paymentdate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", paymentdate=" + paymentdate +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                ", custId=" + custId +
                '}';
    }
}
