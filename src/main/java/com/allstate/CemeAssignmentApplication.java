package com.allstate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CemeAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(CemeAssignmentApplication.class, args);
	}

}
