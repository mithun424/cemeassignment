package com.allstate.rest;

import com.allstate.entities.Payment;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IPaymentController {
    String status();
    int rowcount();
    List<Payment> getAllPayments();
    ResponseEntity<Payment> findById(int id);
    ResponseEntity<List> findByType(String type);
    int save(Payment payment);
}
