package com.allstate.rest;

import com.allstate.entities.Payment;
import com.allstate.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/payment")
public class PaymentController implements IPaymentController {

    @Autowired
    PaymentService paymentService;
    Logger logger = Logger.getLogger(PaymentController.class.getName());

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    @Override
    public String status() {
        logger.info("Payment status method called");
        return "Rest Api is running";
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @Override
    public int rowcount() {
        logger.info("Payment rowcount method called");
        return paymentService.rowcount();
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @Override
    public List<Payment> getAllPayments() {

        return paymentService.getAll();
    }

    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    @Override
    public ResponseEntity<Payment> findById(@PathVariable("id") int id) {
        logger.info("Payment findById method called");
        Payment payment = paymentService.findById(id);

        if (payment == null){
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Payment>(payment, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/findbytype/{type}", method = RequestMethod.GET)
    @Override
    public ResponseEntity<List> findByType(@PathVariable("type") String type) {
        logger.info("Payment findByType method called");
        List<Payment> paymentList= paymentService.findByType(type);

        if (paymentList == null){
            return new ResponseEntity<List>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<List>(paymentList,HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save" ,method = RequestMethod.POST)
    @Override
    public int save(@RequestBody Payment payment) {
        logger.info("Payment save method called");
        return paymentService.save(payment);
    }
}
