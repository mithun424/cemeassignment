package com.allstate.service;

import com.allstate.dao.PaymentDAO;
import com.allstate.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    PaymentDAO paymentDAO;

    @Override
    public int rowcount() {
        return paymentDAO.rowcount();
    }

    @Override
    public List<Payment> getAll() {
        return paymentDAO.getAllPayments();
    }

    @Override
    public Payment findById(int id) {
        return id > 0 ? paymentDAO.findByID(id) : null;
    }

    @Override
    public List<Payment> findByType(String type) {
        return  type != null & !type.trim().isEmpty() ?  paymentDAO.findByType(type) : null;
    }

    @Override
    public int save(Payment payment) {
        return paymentDAO.save(payment);
    }
}
